#!/bin/bash

TMPFOLDER="$(mktemp -d /tmp/webdav-client-installer.XXXXXXXX)"

OWNCLOUD_ZIMLET_CLONE_URL="git://github.com/Zimbra-Community/owncloud-zimlet"
OWNCLOUD_ZIMLET_CLONE_BRANCH="soapServiceBarry"
echo "Download WebDAV Client to $TMPFOLDER"
cd $TMPFOLDER
wget -O ddl.sql https://raw.githubusercontent.com/Zimbra-Community/owncloud-zimlet/soapServiceBarry/onlyoffice/ddl.sql

ONLYOFFICE_PWD=onlyoffice

ONLYOFFICE_DBCREATE="$(mktemp /tmp/onlyoffice-dbcreate.XXXXXXXX.sql)"
cat <<EOF > "${ONLYOFFICE_DBCREATE}"
DROP USER 'ad-onlyoffice_db'@'%';
DROP DATABASE onlyoffice_db;
CREATE DATABASE onlyoffice_db CHARACTER SET 'UTF8'; 
CREATE USER 'ad-onlyoffice_db'@'%' IDENTIFIED BY '${ONLYOFFICE_PWD}'; 
GRANT ALL PRIVILEGES ON onlyoffice_db.* TO 'ad-onlyoffice_db'@'%' WITH GRANT OPTION; 
FLUSH PRIVILEGES; 
EOF

echo "Creating database onlyoffice_db and user"
/opt/zimbra/bin/mysql --force < "${ONLYOFFICE_DBCREATE}"
   
echo "Populating onlyoffice_db please wait..."
/opt/zimbra/bin/mysql onlyoffice_db < $TMPFOLDER/ddl.sql   

# export OWNCLOUD_DB_PASSWORD=${OWNCLOUD_DB_PASSWORD:-"owncloud"}

# echo "Owncloud db password is: ${OWNCLOUD_DB_PASSWORD}"
# OWNCLOUD_DBCREATE="$(mktemp /tmp/owncloud-dbcreate.XXXXXXXX.sql)"
# cat <<EOF > "${OWNCLOUD_DBCREATE}"
# DROP USER 'owncloud'@'%';
# DROP DATABASE owncloud;
# CREATE DATABASE owncloud CHARACTER SET 'UTF8'; 
# CREATE USER 'owncloud'@'%' IDENTIFIED BY '${OWNCLOUD_DB_PASSWORD}'; 
# GRANT ALL PRIVILEGES ON owncloud.* TO 'owncloud'@'%' WITH GRANT OPTION; 
# FLUSH PRIVILEGES; 
# EOF

# echo "Creating database OwnCloud and user"
# /opt/zimbra/bin/mysql --force < "${OWNCLOUD_DBCREATE}"

# echo "Owncload db password is: ${OWNCLOUD_DB_PASSWORD}"
# /opt/zimbra/bin/mysql -u root -e "DROP USER 'owncloud'@'%';"
# /opt/zimbra/bin/mysql -u root -e "DROP DATABASE owncloud;"

# /opt/zimbra/bin/mysql -u root -e "CREATE USER 'owncloud'@'%' IDENTIFIED BY '${OWNCLOUD_DB_PASSWORD}';"
# /opt/zimbra/bin/mysql -u root -e "CREATE DATABASE owncloud CHARACTER SET 'UTF8';"
# /opt/zimbra/bin/mysql -u root -e "GRANT SELECT ON owncloud.* TO 'owncloud'@'%';"
# /opt/zimbra/bin/mysql -u root -e "FLUSH PRIVILEGES;"

# echo "Install daily backup via /etc/cron.daily in /onlyoffice-backup"
# cat <<EOF > /etc/cron.daily/onlyoffice-backup
# #!/bin/bash
# mkdir -p /onlyoffice-backup
# rm /onlyoffice-backup/onlyoffice-`date +%w`.sql
# /opt/zimbra/common/bin/mysqldump -h 127.0.0.1 -P7306 -u'ad-onlyoffice_db' -p'${ONLYOFFICE_PWD}' --add-drop-table onlyoffice_db > /onlyoffice-backup/onlyoffice-`date +%w`.sql
# EOF
# chmod +rx /etc/cron.daily/onlyoffice-backup
